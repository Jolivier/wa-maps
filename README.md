# wa-maps

![screenshot island map](img/screenshot-map-island.png "Island map")

Maps to use with WorkAdventure

![WA and coffee](img/electric-coffee-percolator.png "WA and coffee")
https://workadventu.re/

Thanks to https://www.thecodingmachine.com/ to make this possible.

These maps are made with Tiled https://www.mapeditor.org/

Big thanks to https://opengameart.org/ for the beautifull graphics assets.

## Do you  want to use these maps ?

First, pay attention to licenses.

Then upload the files of the map you want to try / use (json, graphics, sounds and musics) on a webser with **CORS** enable.

See https://workadventu.re/map-building/hosting for technical details.
